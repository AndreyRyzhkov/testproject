package com.example.testproject.services;

import com.example.testproject.TestProjectApplicationTests;
import com.example.testproject.models.Dictionary;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class DictionaryServicesImplTest extends TestProjectApplicationTests {

    @Autowired
    private DictionaryServicesImpl dictionaryServices;

    private Map<String, Integer> getTestMap() {
        Map<String, Integer> testMap = new TreeMap<>();
        testMap.put("test1", 1);
        testMap.put("test2", 2);
        testMap.put("test3", 3);
        return testMap;
    }

    @Test
    @Transactional
    public void testGetAllWordsFromDictionary() {
        List<Dictionary> listTest = dictionaryServices.getAllWordsFromDictionary();
        assertNotNull(listTest);
        listTest = dictionaryServices.getAllWordsFromDictionary();
        assertNotNull(listTest);
    }


    @Test
    @Transactional
    public void testAddMapToDictionary() {
        Map<String, Integer> testMap = getTestMap();
        dictionaryServices.addMapToDictionary(testMap, "https://test.ru");
        List<Dictionary> listTest = dictionaryServices.getAllWordsFromDictionary();
        assertEquals(3, listTest.size());
        int count = 1;
        for (Dictionary dictionary : listTest) {
            assertEquals("test" + count, dictionary.getWord());
            assertEquals(count, dictionary.getValue());
            assertEquals("https://test.ru", dictionary.getUrlSite());
            count++;
        }
    }

    @Test
    @Transactional
    public void testDeleteAllWordFromDictionary() {
        Map<String, Integer> testMap = getTestMap();
        List<Dictionary> listTest = dictionaryServices.getAllWordsFromDictionary();
        assertEquals(0, listTest.size());
        dictionaryServices.addMapToDictionary(testMap, "https://test.ru");
        listTest = dictionaryServices.getAllWordsFromDictionary();
        assertEquals(3, listTest.size());
        dictionaryServices.deleteAllWordFromDictionary();
        listTest = dictionaryServices.getAllWordsFromDictionary();
        assertEquals(0, listTest.size());
    }


    @Test
    @Transactional
    void getDictionary() {
        Map<String, Integer> testMap = dictionaryServices.getDictionary("test");
        assertNotNull(testMap);
    }


    @Test
    @Transactional
    void getError() {
        Map<String, Integer> testMap = dictionaryServices.getDictionary("test");
        assertNotEquals("", dictionaryServices.getError());
        assertEquals("Не введен https://", dictionaryServices.getError());
    }

}

package com.example.testproject.services;

import com.example.testproject.TestProjectApplicationTests;
import com.example.testproject.models.Statistic;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class StatisticServicesImplTest extends TestProjectApplicationTests {

    @Autowired
    private StatisticServicesImpl statisticServices;

    private final String Url = "https://test.ru";

    @Test
    @Transactional
    void getStatistic() {
        List<Statistic> listTest = statisticServices.getStatistic();
        assertNotNull(listTest);
    }

    @Test
    @Transactional
    void addStatistic() {
        Statistic testStatistic = statisticServices.addStatistic(Url);
        assertNotNull(testStatistic);
        assertEquals(Url, testStatistic.getUrlSite());
    }

    @Test
    @Transactional
    void deleteAllStatistic() {
        Statistic testStatistic = statisticServices.addStatistic(Url);
        assertEquals(1, statisticServices.getStatistic().size());
        statisticServices.deleteStatistic(testStatistic.getId());
        assertEquals(0, statisticServices.getStatistic().size());
    }
}

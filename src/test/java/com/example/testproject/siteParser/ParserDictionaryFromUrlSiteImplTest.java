package com.example.testproject.siteParser;

import com.example.testproject.TestProjectApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class ParserDictionaryFromUrlSiteImplTest extends TestProjectApplicationTests {


    @Autowired
    private ParserDictionaryFromUrlSiteImpl parserDictionaryFromUrlSite;

    @Test
    void getBodyFromSiteUrl() {
        String body = parserDictionaryFromUrlSite.getBodyFromSiteUrl("https://yandex.ru");
        assertNotNull(body);
        parserDictionaryFromUrlSite.getDictionaryFromSite("https://yandex6666.ru");
        assertEquals("urlsite адрес не существует", parserDictionaryFromUrlSite.getError());
    }

    @Test
    void getArrayWords() {
        String[] array = parserDictionaryFromUrlSite.getArrayWords("https://yandex.ru");
        assertNotNull(array);
        assertNotEquals(0, array.length);
        array = parserDictionaryFromUrlSite.getArrayWords("https://yandex6666.ru");

    }

    @Test
    void getDictionaryFromSite() {
        Map<String, Integer> mapTest = parserDictionaryFromUrlSite.getDictionaryFromSite("https://yandex.ru");
        assertNotNull(mapTest);
        assertNotEquals(0, mapTest.size());
        parserDictionaryFromUrlSite.getDictionaryFromSite("yandex.ru");
        assertEquals("Не введен https://", parserDictionaryFromUrlSite.getError());

    }

}

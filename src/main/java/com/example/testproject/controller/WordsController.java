package com.example.testproject.controller;


import com.example.testproject.models.Dictionary;
import com.example.testproject.models.Statistic;
import com.example.testproject.services.DictionaryServices;
import com.example.testproject.services.StatisticServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@RequestMapping()
public class WordsController {

    private final DictionaryServices dictionaryServices;
    private final StatisticServices statisticServices;


    @GetMapping("/parser")
    public String getParserPage(Model model) {
        List<Statistic> listStatistics = statisticServices.getStatistic();
        model.addAttribute("listStatistics", listStatistics);
        return "parserPage";
    }


    @PostMapping("/parsing")
    public String addProduct(@RequestParam(value = "urlSite") String urlSite, Model model, RedirectAttributes redirectAttributes) {
        Map<String, Integer> dictionary = dictionaryServices.getDictionary(urlSite);
        if (!dictionaryServices.getError().equals("")) {
            redirectAttributes.addFlashAttribute("error", dictionaryServices.getError());
            return "redirect:/parser";
        }
        dictionaryServices.addMapToDictionary(dictionary, urlSite);
        statisticServices.addStatistic(urlSite);
        System.out.println(urlSite);
        model.addAttribute("urlSite", urlSite);
        model.addAttribute("dictionary", dictionary);
        return "/dictionary";
    }

    @PostMapping("/statistic/{statistic-id}/delete")
    public String deleteStatistic(@PathVariable("statistic-id") Long statisticsId) {
        statisticServices.deleteStatistic(statisticsId);
        return "redirect:/parser";
    }

    @PostMapping("deleteAll")
    public String deleteAll() {
        dictionaryServices.deleteAllWordFromDictionary();
        statisticServices.deleteAllStatistic();
        return "redirect:/parser";
    }

    @GetMapping("/dictionary")
    public String getDictionaryPage(Model model) {
        List<Dictionary> dictionaryList = dictionaryServices.getAllWordsFromDictionary();
        model.addAttribute("dictionaries", dictionaryList);
        return "dictionariesPage";
    }


}

package com.example.testproject.siteParser;

import lombok.*;
import org.springframework.stereotype.Component;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Component
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ParserDictionaryFromUrlSiteImpl implements ParserDictionaryFromUrlSite {


    private static final Logger log = Logger.getLogger(ParserDictionaryFromUrlSiteImpl.class);

    private String error;


    public String getBodyFromSiteUrl(String urlSite) {
        Document document;
        try {
            document = Jsoup.connect(urlSite).get();
        } catch (IOException e) {
            //   log.warn("url адрес не существует " + e.getMessage());
            log.warn("urlSite адрес не существует " + e.getMessage());
            error = "urlsite адрес не существует";
            return null;
        }
        return document.body().text();
    }

    public String[] getArrayWords(String urlSite) {
        String body = getBodyFromSiteUrl(urlSite);

        if (body == null) {
            return new String[0];
        }
        body.replaceAll("/n", "").trim();
        body = body.replaceAll("[^a-zA-Zа-яёА-ЯЁ ]", "").replaceAll("( +)", " ").trim();
        return body.split(" ");
    }

    public Map<String, Integer> getDictionaryFromSite(String urlSite) {
        log.info("Парсинг сайта " + urlSite);
        Map<String, Integer> dictionary = new HashMap<>();
        error = "";

        if (!(urlSite.startsWith("http://") || urlSite.startsWith("https://"))) {
            log.warn("Отсутствует HTTP — протокол ->" + urlSite);
            error = "Не введен https://";
        } else {
            String[] arraySting = getArrayWords(urlSite);
            for (String tempSting : arraySting) {
                if (dictionary.containsKey(tempSting)) {
                    dictionary.put(tempSting, dictionary.get(tempSting) + 1);
                } else {
                    dictionary.put(tempSting, 1);
                }
            }
        }
        if (error.equals("")) {
            log.info("Адрес сайта " + urlSite + " полность обработан");
        }
        return dictionary;
    }
}

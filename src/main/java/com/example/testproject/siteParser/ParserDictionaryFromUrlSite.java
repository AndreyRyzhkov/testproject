package com.example.testproject.siteParser;

import java.util.Map;

public interface ParserDictionaryFromUrlSite {
    Map<String, Integer> getDictionaryFromSite(String url);

    String getError();
}

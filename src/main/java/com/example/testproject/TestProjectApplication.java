package com.example.testproject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import javax.swing.text.*;
import java.io.IOException;

@SpringBootApplication
public class TestProjectApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(TestProjectApplication.class, args);
    }

}

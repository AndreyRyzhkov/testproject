package com.example.testproject.services;

import com.example.testproject.models.Dictionary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface DictionaryServices {
    List<Dictionary> getAllWordsFromDictionary();

    void addMapToDictionary(Map<String, Integer> dictionary, String urlSite);

    Map<String, Integer> getDictionary(String urlSite);

    String getError();

    void deleteAllWordFromDictionary();


}

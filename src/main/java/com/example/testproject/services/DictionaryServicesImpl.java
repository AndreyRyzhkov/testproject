package com.example.testproject.services;

import com.example.testproject.siteParser.ParserDictionaryFromUrlSite;
import com.example.testproject.models.Dictionary;
import com.example.testproject.repositories.DictionaryRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@Getter
@RequiredArgsConstructor
public class DictionaryServicesImpl implements DictionaryServices {

    private final DictionaryRepository dictionaryRepository;
    private final ParserDictionaryFromUrlSite parserDictionaryFromUrlSite;


    @Override
    public Map<String, Integer> getDictionary(String urlSite) {
        return parserDictionaryFromUrlSite.getDictionaryFromSite(urlSite);
    }


    @Override
    public List<Dictionary> getAllWordsFromDictionary() {
        List<Dictionary> dictionaryList = (List<Dictionary>) dictionaryRepository.findAll();
        return dictionaryList;
    }

    @Override
    public void addMapToDictionary(Map<String, Integer> dictionary, String urlSite) {
        for (Map.Entry<String, Integer> entry : dictionary.entrySet()) {
            Dictionary tempElementDict = Dictionary.builder()
                    .urlSite(urlSite)
                    .word(entry.getKey())
                    .value(entry.getValue())
                    .build();
            dictionaryRepository.save(tempElementDict);
        }
    }

    @Override
    public void deleteAllWordFromDictionary() {
        dictionaryRepository.deleteAll();
    }

    @Override
    public String getError() {
        return parserDictionaryFromUrlSite.getError();
    }
}

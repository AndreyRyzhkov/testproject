package com.example.testproject.services;

import com.example.testproject.models.Statistic;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface StatisticServices {

    List<Statistic> getStatistic();

    Statistic addStatistic(String urlSite);

    void deleteStatistic(Long statisticId);

    void deleteAllStatistic();


}

package com.example.testproject.services;

import com.example.testproject.models.Statistic;
import com.example.testproject.repositories.StatisticRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StatisticServicesImpl implements StatisticServices {

    private final StatisticRepository statisticRepository;

    @Override
    public List<Statistic> getStatistic() {
        return (List<Statistic>) statisticRepository.findAll();
    }

    @Override
    public Statistic addStatistic(String urlSite) {
        Statistic statistics = Statistic.builder()
                .localDate(LocalDate.now())
                .time(LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute())
                .urlSite(urlSite)
                .build();
        statisticRepository.save(statistics);
        return statistics;
    }

    @Override
    public void deleteStatistic(Long statisticId) {
        statisticRepository.deleteById(statisticId);
    }

    @Override
    public void deleteAllStatistic() {
        statisticRepository.deleteAll();
    }


}

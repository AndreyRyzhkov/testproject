Парсер страниц HTML

О работе приложения

Данное приложение разработано в Intellij IDEA 2021.2.3 (Ultimate Edition) на языке Java 17 (сборка пректа Maven).
Были использованы следующие технологии:

- Spring Framework (Spring Boot, Spring Data, Spring WEB, Spring MVC).
- Hibernate
- PostgreSQL
- Thymeleaf
- Bootstrap

Порядок запуска приложения:

- В PostgreSQL создать пользователя с именем postgres с паролем 123456.
- Создать базу данных dictionary_html. 
- Таблицы dictionary и statistic создаються с помошью Hibernate.
- Загрузить проект в Intellij IDEA из папки testProject.
- Скомпилируйте и запустите приложение из Application class.
- Вы можете изменить настройки доступа к базе данных в файле настройки application.properties:
- ------------------------------------------------------------------
  spring.datasource.driver-class-name=org.postgresql.Driver
  spring.datasource.username=postgres
  spring.datasource.password=123456
  spring.datasource.url=jdbc:postgresql://localhost:5432/dictionary_html
  ---------------------------------------------------------------------------

Логирование производится в файл log_file.log в корень проекта. 
Вы можете изменить эту настройку в файле log4j.properties
--------------------------------------------------------------
  log4j.appender.file.File=log_file.log
-----------------------------------------------------------------

АВТОР:
Рыжков Андрей Николаевич, drongo13@gmail.com, +79061437506